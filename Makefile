.DEFAULT_GOAL := help

.PHONY: venv
venv: ## Create a Python virtualenv.
	python -m venv venv


.PHONY: install
install: ## Install the dependencies.
	python -m pip install -r requirements.txt


.PHONY: dev
dev: ## Install the development dependencies.
	python -m pip install -r requirements-dev.txt


.PHONY: deps
deps: ## Upgrade `requirements(-dev).txt` from `.in` files.
	python -m piptools compile requirements.in --upgrade --quiet
	python -m piptools compile requirements-dev.in --upgrade --quiet


.PHONY: serve
serve: ## Launch a local server to serve the content.
	@mkdocs serve


.PHONY: build
build: ## Build the website for production.
	@mkdocs build


.PHONY: help
help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

define PRINT_HELP_PYSCRIPT # start of Python section
import re, sys

output = []
# Loop through the lines in this file
for line in sys.stdin:
	# if the line has a command and a comment start with
	#   two pound signs, add it to the output
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		output.append("\033[36m%-10s\033[0m %s" % (target, help))
# Sort the output in alphanumeric order
output.sort()
# Print the help result
print('\n'.join(output))
endef
export PRINT_HELP_PYSCRIPT # End of python section
