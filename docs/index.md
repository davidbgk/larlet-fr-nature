# Accueil

Inspiré par le partage de [mes bouts de codes](https://code.larlet.fr/), et encouragé par des discussions ici et là au sujet du matériel léger, des astuces nature, des contraintes personnelles, … j’avais envie de documenter certaines choses sur un espace dédié.

Cela fait 5 ans que je [raconte certaines sorties](https://larlet.fr/david/bushcraft/) (en 2021, c’était [plutôt par ici](https://larlet.fr/david/2021/foret/) et en 2023 [par là](https://larlet.fr/david/2023/foret/)).
Le temps d’apprendre pas mal de choses sur mon environnement et sur moi-même.
Je vais balader et bivouaquer dans la forêt boréale, au nord de Montréal, QC (Laurentides, Lanaudières).

Je ne sais pas encore jusqu’où ça ira, le chemin est ainsi fait qu’il se construit en marchant.

À défaut de savoir où je vais, si je devais définir cet espace en creux par ce qu’il n’est pas :

* ce n’est pas vraiment du camping car je fuis les emplacements (populaires) ;
* ce n’est pas vraiment du bushcraft car j’emporte pas mal d’équipement ;
* ce n’est pas du tout de la chasse même si j’emprunte souvent leur matériel chaud/silencieux ;
* ce n’est pas un guide de survie(valisme), davantage de sur-vie grâce à la forêt ;
* ce n’est pas pour me montrer mais pour partager/inspirer.

## Sections


<div class="grid cards" markdown>

- [![Un réchaud à bois en cours d’usage](images/half/feu-rechaud.jpg) :fire: __Le feu__](feu.md)
- [![Tentative de couchage sur un raft retourné](images/half/couchage-raft.jpg) :sleeping_accommodation: __Le couchage__](couchage.md)
- [![Un tipi dans la neige](images/half/abri-tipi.jpg) :camping: __L’abri__](abri.md)
- [![Une main d’enfant qui va se saisir de bleuets](images/half/cueillette-bleuets.jpg) :blueberries: __La cueillette__](cueillette.md)
- [![Photo d’un lac gelé](images/half/securite-lac-gele.jpg) :helmet_with_cross: __La sécurité__](securite.md)
- [![Un feu allumé sur la neige](images/half/froid-feu-allumage.jpg) :cold_face: __Le froid__](froid.md)
- [![Des pieds, chaussettes et chaussures qui sèchent autour d’un réchaud à bois](images/half/hygiene-pieds-mouilles.jpg) :poo: __L’hygiène__](hygiene.md)
- [![Plat de riz + haloumi + sésame.](images/half/nourriture-riz-haloumi-sesame.jpg) :fork_knife_plate: __La nourriture__](nourriture.md)
- [![Un tabouret avec des myrtilles dessus et nos pieds dessous](images/half/enfant-5ans.jpg) :family_man_boy: __L’enfant__](enfant.md)
- [![Une photo avec le filtre](images/half/eau-filtre.jpg) :sweat_drops: __L’eau__](eau.md)
- [![J’observe quelque chose avec des jumelles](images/half/habillage-mitaines.jpg) :gloves: __L’habillage__](habillage.md)
- [![Je tire un traîneau, en montée, avec l’enfant dedans](images/half/entrainement-hiver.jpg) :runner: __L’entraînement__](entrainement.md)

<!--
- [:compass: __L’orientation__](orientation.md)
- [:bike: __Le vélo__](velo.md)
- [:book: __Les histoires__](histoires.md)
-->
</div>

!!! warning "Style ?"

    Il arrive que je prenne un ton un peu ascendant/autoritaire sur certaines sections.
    Il s’agit surtout pour moi de m’auto-asséner ces conseils pour m’en rappeler !

!!! info
    Je mets des liens vers des produits.
    Ils sont généralement très chers (et non sponsorisés) mais je me justifie cela par les conditions d’emploi et l’investissement sur de nombreuses années.
    Des alternatives moins dispendieuses existent souvent (oui, je vais aussi utiliser du vocabulaire québécois).

Il y a une page de [Colophon](colophon.md) qui détaille les outils pour construire cet espace.

==Bonne balade ! :material-hand-wave:==
