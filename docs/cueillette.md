# La cueillette


!!! info "Contexte"

    Il y a cette sensation étrange d’absorber la forêt tout en étant dans la forêt.
    Une forme d’immersion ultime.

    C’est une activité que j’aimerais davantage pratiquer en arpentant la nature.
    Beaucoup d’ouvrage sont récemment sortis à ce sujet au Québec.

!!! warning "Rester raisonnable"

    La forêt boréale donne une idée d’immensité, d’infinité.
    Malheureusement, il suffit de quelques années pour déjà s’apercevoir de la déterioration de certains lieux.
    Il en faut pour tout le monde (incluant un futur moi/vous).


## Bleuets

C’est le plus facile à collecter, de mi-juillet à mi-août environ.

Très peu de risques de se tromper, si ce n’est avec une autre plante qui partage le même milieu et la même taille/couleur de fruit mais ceux-ci sont au bout de longues tiges (vs. sur les branches pour les bleuets).

Selon les coins, on le trouve en abondance.
Il est parfois utile de longer une berge ou d’accoster une île en canot pour remonter un bon filon. 

<figure markdown>

![Une main d’enfant qui va se saisir de bleuets](images/cueillette-bleuets.jpg)

<figcaption>
    Le principal risque est de perdre son chemin en suivant celui des 
    bleuets…
</figcaption>
</figure>


## Framboises

Un autre petit fruit facile à trouver sans crainte.
La principale difficulté, ce sont les bibites qui pullulent à ses côtés !

Il vaut mieux être bien couvert en mode apiculteur pour approcher de ces délicieuses baies.
Il peut aussi y avoir pas mal d’insectes dessus, à vérifier avant d’engloutir.


## Épinette

En infusion, quelques pousses de l’année collectées au bout des branches et tout à coup cette eau vaseuse ou cette neige brûlée prennent une autre saveur !

<figure markdown>

![Une infusion d’épinette](images/cueillette-epinette.jpg)

<figcaption>
    Il est possible l’hiver de mettre les branches directement dans le thermos.
</figcaption>
</figure>

## Chaga

Il s’agit d’un champignon noir qui pousse sur le tronc des bouleaux.
Il n’a pas une forme de champignon mais ressemble à une cicatrice noire.
On ne peut pas se tromper et il vaut mieux le ramasser en hiver car il s’est alors chargé des nutriments de l’arbre.
Aussi, il vaut mieux que l’arbre soit encore vivant au moment de la cueillette.
C’est relativement difficile à ramasser car c’est très dur (qui plus est en hiver).

Ça se consomme en infusion en essayant de ne pas le faire bouillir pour conserver les éléments bénéfiques intacts.
Une fois de nouveau sec, il peut être réutilisé quelques fois.

!!! warning "Café…"
    
    Pour être tout à fait honnête, beaucoup le comparent au café et je n’aime pas ça donc la comparaison doit être bonne.


## Thé du labrador

!!! info
    Il ne s’agit pas d’un thé à proprement parler.

Il se déguste en infusion comme du thé.
C’est un goût très léger qui est à la mode — ce qui lui confère immédiatement de très nombreuses vertus.
Plus sérieusement, les Premières Nations l’utilisaient beaucoup, notamment dans le calumet mais aussi en tant que plante médicinale.

<figure markdown>

![Une récolte de thé du labrador](images/cueillette-the-labrador.jpg)

<figcaption>
    Récolte effectuée en août 2022, il est davantage indiqué de la faire plus tôt dans l’été.
</figcaption>
</figure>

Il ne faut pas trop en abuser car il contient des alcaloïdes toxiques à forte dose.

!!! warning "Attention"
    
    Il peut être confondu avec une plante toxique.
    La principale distinction réside dans le fin duvet qui est sous les feuilles et qui passe de blanc à brun au cours de l’été.
