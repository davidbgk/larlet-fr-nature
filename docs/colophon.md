# Colophon

Ce site est construit avec [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) et
le fichier de configuration est le suivant :

```yaml title="mkdocs.yml" linenums="1"
--8<-- "mkdocs.yml"
```

1.  :man_gesturing_no: désactivation des polices Google


Il utilise la police [tulia](https://www.1001fonts.com/tulia-font.html) par [Rebekka Marleaux](http://www.rebekkafalke.de)
 définie ainsi :

```css title="fonts.css" linenums="1" hl_lines="2 10 19"
--8<-- "docs/static/stylesheets/fonts.css"
```
