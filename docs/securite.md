# La sécurité

!!! info "Contexte"

    Quelques conseils de sécurité sur des situations que je n’ai pas forcément vécues.
    Difficile de savoir si c’est parce que j’applique ces recommandations ou si c’est par pure chance.
    Nous avons tou·tes nos expériences respectives qui nous poussent à considérer davantage un aspect de la sécurité et moins un autre.

!!! warning "Éviter le sur-accident"

    En particulier en partant seul·e, l’accident peut arriver mais une
    mauvaise décision conduisant à un sur-accident et là ça devient vite
    critique.
    Recommandation n°1 en cas de pépin : manger et boire avant de
    faire un choix engageant.


## Communication

Une chose importante pour réduire le périmètre des recherches éventuelles est de communiquer l’endroit où l’on va, le parcours envisagé, les solutions de repli possibles.
Et de les mettre à jour aussi souvent que nécessaire au gré de l’avancé du parcours.

C’est relativement simple à mettre en pratique avec un peu de réseau et ça peut littéralement vous sauver la vie.
Il existe des dispositifs plus techniques/avancés comme Spot, etc.


## Petits bobos

Ma trousse à pharmacie varie en fonction des saisons et de l’engagement
de l’activité prévue.

J’ai quand même toujours :

* 2/3 pansements (+2/3 pour les ampoules) ;
* une sorte de crème composée d’huile d’olive, de sève de bouleau et de cire d’abeille qui me sert à tout (coups de soleil, coupures, frottements, etc) ;
* des pastilles pour la gorge ;
* du paracétamol (jamais utilisé) ;
* un baume à lèvres.

Ce à quoi j’ajoute en hiver des chaufferettes et en été des tire-[tiques](#retirer-une-tique).

Après, si c’est engagé, je peux ajouter pas mal de choses, d’une atelle à une compresse coagulante, etc.
Il vaut mieux essayer de partir au moins à deux aussi !


## Être déshydraté·e

C’est le plus courant : mauvaise gestion de son eau (interne et externe), accident de gourde, source tarie, etc.
Il est parfois difficile de s’en rendre compte avant que ce soit un peu trop tard.
Connaître et être à l’écoute de son corps est assurémment un avantage dans ces situations.

La première chose que je fais dans ces cas là c’est de réduire l’allure puis de regarder le relief pour trouver le prochain vallon ou lac.
Arrêter d’uriner peut aussi être une option, il y a un mécanisme corporel de récupération de ce liquide en cas de déshydratation importante.


## Être en hypothermie

!!! info

    Une page [dédiée au froid](froid.md) existe.

Rester actif, protéger les extrêmités, emprisonner la chaleur corporelle, s’abriter des éléments, beaucoup de choses classiques pour éviter l’hypothermie. Mais que faire une fois qu’elle a commencé à poindre son nez gelé ?

En général, ça commence par les extrêmités.
Récupérer des doigts gelés c’est long et perdre la dextérité devient rapidement problématique pour allumer un feu par exemple…
Parfois, je trouve que les gants/moufles/mitaines/chaussettes froides et forcément un peu humides sont contre-productives et il vaut mieux mettre les mains/pieds l’un·e contre l’autre dans un endroit abrité.
Bien remuer les bras (en utilisant des bâtons par exemple) est un moyen d’amener le sang chaud jusqu’au bout des membres.

Si les pieds sont gelés pour de vrai, la seule option est de les faire tremper dans une eau tiède le temps qu’ils retrouvent leur vitalité.
C’est rarement possible en pleine nature gelée… mais s’il y a un refuge sur la route ça devient envisageable !

Lorsque tout le corps est concerné, c’est plus critique.
Surtout en n’ayant plus l’énergie d’être en activité ou en étant blessé·e.
Si on est en sortie bivouac, se mettre à l’abri _et_ dans son duvet le temps de se réchauffer peut être une stratégie.
L’autre option est de faire un [feu](feu.md) mais c’est non trivial [en hiver/sur la neige](froid.md#feu).


## Retirer une tique

Je ne me suis pas encore fait mordre par une tique.
C’est une de mes peurs car la maladie de Lyme est extrêmement présente
au Québec (au point que le traitement préventif est administré avant
même que des symptômes n’apparaissent).
Il se trouve que les ongulés sont le principal réservoir de la tique et nous empruntons les mêmes chemins.

<figure markdown>

![Un chemin au milieu des fougères](images/securite-tique.jpg)

<figcaption>
    Comment traverser ces fougères en étant serein ?, août 2022.
</figcaption>
</figure>


Les choses qui ont peut-être aidé :

* utiliser des pantalons Fjällräven en G-1000 qui sont assez réputés
  pour réduire l’accrochage ;
* inspecter consciencieusement et régulièrement les coins connus
  (aine, aisselles, arrières d’oreilles, etc) ;
* toujours porter des habits longs et clairs (même par 35°C…) 
  à cause des bibites ;
* fermer la tente ;
* être peu poilu (??)

!!! warning "Outils"

    Avoir un [tire-tique](https://fr.wikipedia.org/wiki/Tire-tique), ça peut aider pour un retrait moins risqué.
    En effet, la tique régurgite ses toxines par la tête au moment du retrait… et c’est la voie de transmission de la maladie de Lyme.
    Tirer dessus avec ses ongles est plutôt contre-indiqué.


## Traverser un lac gelé

!!! info

    Une page [dédiée au froid](froid.md) existe.

Je ne suis pas passé à travers un lac gelé et honnêtement si ça arrive il est peu probable que je sois ensuite en mesure de le raconter…

Je vérifie plusieurs choses avant la traversée :

* la météo des jours précédents, s’il y a un redoux c’est à surveiller ;
* la qualité de la glace **sous** la couche de neige, c’est parfois trompeur de voir une bonne couche blanche mais en-dessous ça a commencé à fondre ;
* en début et fin de saison, je traverse des lacs que je connais et surtout dont je connais le sens d’écoulement pour éviter les entrées/sorties d’eau ;
* analyser les traces des animaux (et des skidoo…) peut aider, surtout s’il y a un trou à la fin :sweat_smile: ;
* commencer par parcourir le bord et analyser les bruits.

J’ai l’espoir qu’en mettant mes bâtons en travers, ça empêche l’immersion complète si ça craque. C’est important l’espoir.

<figure markdown>

![Photo d’un lac gelé](images/securite-lac-gele.jpg)

<figcaption>
    Traversera ou pas ?, décembre 2019.
</figcaption>
</figure>

!!! hint "Outils"

    Il existe des sortes de colliers avec deux pointes permettant de se
    sortir de là une fois passé à travers (en théorie du moins).
    Cela peut être confectionné à la main avec deux couteaux reliés par
    une corde qui passe à travers les manches (toujours théorique).
    Je me rappelle avoir emporté un piolet pour ça une fois mais 
    ça s’est surtout avéré pratique pour déneiger les sardines
    de la tente…


## Franchir un torrent de fonte

Alors ça par contre, j’ai fait.
Un trop grand nombre de fois à mon goût.

On est fin avril et j’ai l’espoir qu’il n’y ait plus de neige, je laisse les raquettes dans le coffre et je pars en baskets (oui, car il fait déjà chaud en plus).
Le problème c’est que chaque vallon a encore 80 cm de neige bien lourde et collante ainsi qu’un torrent violent qu’on entend sous la neige.

Une fois passé à travers, l’eau à 0°C remplie les chaussures et seuls les bâtons peuvent un peu vous sauver la mise.
La sensation n’est pas agréable et l’appréhension du prochain vallon ne va pas vous quitter.
En connaissant le coin en été, ça peut être utile de repérer les passages avec des rochers.

Chaque printemps je me fais avoir.
J’espère m’en souvenir un peu mieux en le consignant ici.

!!! info "Pieds mouillés"

    Il existe [une section dédiée](hygiene.md#les-pieds) dans la page relative à l’hygiène.

## Croiser un ours

Cela fait maintenant 5 ans que je me balade dans la forêt et je 
n’ai toujours pas croisé d’ours. Donc c’est plutôt
« Comment peut-être ne pas croiser d’ours » :shrug:.

Quelques hypothèses en vrac :

* ne pas faire griller de viande ;
* ne pas dormir à des endroits trop populaires ;
* uriner tout autour du campement (incluant l’arbre qui tient le sac en hauteur) ;
* bien emballer la nourriture ;
* être un piètre pêcheur.

Choses que j’ai fait mais que je ne fais plus :

* trimbaler un « clochette à ours » ;
* suspendre ma nourriture à un fil ;
* être bruyant intentionellement ;
* stresser au moindre bruit autour de la tente ;
* me balader avec une bombe au poivre.

Une bonne pratique est également d’avoir un coin cuisine éloigné du coin couchage.
Cela évite de se réveiller nez-à-nez avec un animal sauvage.


## Retirer une sangsue

On est toujours dans le théorique mais j’en vois pas mal quand je me baigne donc je me suis un peu renseigné et le plus pratique semble être de la décoller avec une carte de crédit ou équivalent.

Il n’y a pas de maladie particulière associée.

!!! info

    C’est super joli quand ça nage/vole au bord de l’eau :star_struck:


## Échapper à une avalanche

Aucun conseil à donner, il n’y a pas de montagnes au Québec :sob:.


## Écouter les loups

En hiver, lorsque les sons portent loin, il n’est pas rare d’entendre des hurlements au loin.
Tant qu’ils sont éloignés, le niveau de stress de se sentir être une proie est gérable.
Lorsqu’au réveil, des traces fraîches sont visibles à une quinzaine de mètres du campement… il faut apprendre à respirer calmement.

<figure markdown>

![Ma main à côté d’une empreinte de gros canidé](images/securite-loups.jpg)

<figcaption>
    Loup ou coyote ?, mars 2022.
</figcaption>
</figure>

!!! book "Peur nocturne"

    Octobre 2017, je m’engage pour la première fois sur un sentier québécois avec l’intention de faire un bivouac en pleine nature.
    Après une longue marche, me voilà à côté d’un lac avec le sentiment d’être isolé comme jamais.

    Le soir tombe, je fais ma cuisine sur un petit foyer puis je vais me blottir dans ma couverture bien trop légère pour la saison.
    Cela me garde éveillé pendant un bon moment et j’entends au loin un hurlement de loup.
    Je souris, l’imaginaire canadien, d’aventure, d’immersion me saisit.
    Quelques secondes plus tard, j’entends une réponse venant d’une autre direction.
    Pour l’européen fraîchement débarqué que je suis, c’est tellement exotique !

    Quand soudain, une réponse très proche, une centaine de mètres tout au plus.
    Mon sang de proie ne fait qu’un tour, me voilà bien réveillé, debout, à envisager tous les moyens de défenses possibles.
    Je décide de rallumer un feu à la va-vite en gardant la hache à proximité.
    Feu que je vais passer la nuit à alimenter en me retournant à chaque craquement.

    Avec le recul et l’expérience de vrais hurlements, c’était probablement des huards se répondant de lacs en lacs.
    Encore une peur de débutant qu’il a fallu dépasser.

    Si vous voulez la [version complète de cette sortie](https://larlet.fr/david/blog/2017/into-the-wild/) (en anglais).


## Naviguer sur un lac

Il y a deux choses requises pour être en conformité avec la loi québécoise/canadienne :

* une corde de 30 pieds/15 mètres ;
* une veste de flottaison.

Mon embarcation est très lente et peu optimisée pour des météo difficiles comme un vent de face.
Son principal atout est la légèreté (et la portabilité associée) mais c’est à double tranchant car le retournement est tout à fait possible.
D’une certaine manière, cela me pousse à être plus prudent et à opter pour des campements ayant une solution de repli pédestre en fonction des prévisions météorologiques.

<figure markdown>

![Dans un raft, sur un lac](images/securite-raft.jpg)

<figcaption markdown>
_I’m on a boat!_, c’est surtout pour montrer la corde d’amarrage/tractage, septembre 2018.
</figcaption>
</figure>
