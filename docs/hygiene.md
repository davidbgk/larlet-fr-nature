# L’hygiène

!!! info "Contexte"

    On n’est pas tou·tes sensibles aux mêmes désagréments de la vie loin de la civilisation.
    C’est aussi à adapter en fonction du nombre de jours.


## Les pieds

Je commence par là car c’est mon principal moyen de locomotion.

Le plus important c’est de soigner dès le premier ressenti de frottement, pas après cette montée, pas au moment du repas, **maintenant**.
Une ampoule peut être vraiment handicapante, surtout sur plusieurs jours.
Les pansements actuels dédiés sont plutôt bons.

Bon, encore mieux, c’est de prévenir cela en ayant des chaussures usagées, à la bonne taille avec des chaussettes que je recommande en mérinos (odeurs, thermicité, longévité).
Sur de grosses bottes de neige, il est envisageable de doubler les chaussettes.

!!! hint "Les pieds dans l’eau"

    Une fois que la chaussure est mouillée (neige, marais, accostage loupé en raft…), il est très long de la faire sécher.
    Alterner deux paires de chaussettes avec une qui récupère l’humidité de la chaussure et l’autre qui la libère au-dessus du feu est possible (bien que fastidieux mais ça laisse le temps d’internaliser la source de l’échec).

    J’emporte toujours deux paires de chaussettes de rechange car il m’en faut une sèche pour dormir ensuite.
    Une autre option est de partir avec 2 sacs à compost à mettre entre la chaussure mouillée et la chaussette sèche mais alors attention ça glisse pas mal !

<figure markdown>

![Des pieds, chaussettes et chaussures qui sèchent autour d’un réchaud à bois](images/hygiene-pieds-mouilles.jpg)

<figcaption>
    Un exemple de séchage avec des pieds gelés.
</figcaption>
</figure>


## Les aisselles

Plongeon dans un lac, pas mieux.
Parfois même avec le t-shirt trempé de sueur pour qu’il se rince un peu au passage.

Lorsqu’il fait un peu plus froid, les lingettes de la marque Wysi Wipes ou équivalent (un [exemple ici](https://canadianpreparedness.ca/collections/toilet-paper-tablets-1/products/toilet-paper-tablets-mini-roll)) me suffisent.
Mais je suis très souvent seul.

Certain·es préfèrent les lingettes pour bébé.
Je trouve que ça pue.
Et que ça pourrait attirer des plus ou moins grosses bêtes.

!!! hint "Pro-tip"

    Se raser sous les bras (ou au moins couper très court), ça réduit de beaucoup les odeurs.
    J’ai découvert ça quand le frottement des sports du triathlon m’était devenu insupportable.
    En bonus, c’est bien plus facile pour repérer [les tiques.](securite.md#retirer-une-tique)



## Les dents

Une toute petite brosse à dent, comme [celle de Zpacks](https://zpacks.com/products/ultralight-travel-toothbrush), c’est quand même appréciable. 
Surtout selon ce qu’il y a au repas…

C’est mieux de prendre un dentifrice biodégradable et de ne pas laisser trop de traces sur les feuilles avoisinnantes. Toujours ce truc de limiter les traces (visibles).

<figure markdown>

![Un camembert sur un feu ouvert](images/hygiene-dents.jpg)

<figcaption>
    La dernière fois que j’ai oublié ma brosse à dent et qu’il y avait un camembert rôti au menu… je m’en suis rappelé le lendemain !
</figcaption>
</figure>


## Faire pipi

Il n’y a que le premier du jour où je fais un effort pour être éloigné du campement + de toute source d’eau.
Une centaine de mètres si ma vessie le permet.

J’ai pris pour habitude sinon de définir le périmètre du campement en répartissant sur les troncs d’arbres alentours à hauteur de truffe.
Peut-être que ça me protège [des ours](securite.md#croiser-un-ours) et autres grosses bêtes.

Pour les personnes de sexe féminin, je sais qu’il existe des dispositifs permettant de ne pas avoir à se découvrir/s’accroupir de trop (Freshette, Whiz Freedom, GoGirl, etc).


## Faire caca

Il y a des ouvrages entiers sur le sujet (bon ok, au moins un).

Le papier toilette laisse des traces pendant pas mal de temps, je préfère utiliser des lingettes de la marque Wysi Wipes ou équivalent (un [exemple ici](https://canadianpreparedness.ca/collections/toilet-paper-tablets-1/products/toilet-paper-tablets-mini-roll)) qui prennent vraiment peu de place/poids.
Il suffit d’un peu d’eau (toujours garder un fond de gourde !) et ça prend la taille d’un mouchoir un peu humide parfait pour rester propre.
C’est même plus sain que du papier toilette et je me suis mis à l’utiliser à la maison…

Pour le déroulé :

1. imbiber les lingettes d’eau ;
2. creuser autant que possible avec le talon au pied d’un arbre ;
3. faire ce qu’il y a à faire ;
4. si nécessaire, faire un premier essuyage avec un mouchoir ;
5. utiliser en moyenne deux lingettes (s’il en reste ça peut permettre de se nettoyer ailleurs) ;
5. recouvrir pour que rien ne reste visible ;
6. planter un bâton pour les suivant·es.

Un petit flacon de gel hydro-alcoolique pour garder les mains propres et le tour est joué !


!!! book "Un caca fumant"

    Il y a quelques années, je lis qu’il est quand même plus écologique(?) de brûler son papier toilette si on ne veut vraiment pas laisser de traces (ce qui n’est peut-être pas si évident en terme de bilan carbone mais passons).
    Je commence à mettre cela en pratique en hiver car je me dis qu’une fois la neige fondue ça va peut-être devenir assez visible.

    Arrive le printemps, où l’on passe en quelques jours de plusieurs pieds de neige à un sol très sec recouvert des feuilles de l’automne précédent.
    Me voilà avec mon briquet, en train d’enflammer mon papier toilette.
    Je m’y reprends à plusieurs fois car il y a un peu de vent (grossière erreur) mais ça finit par prendre assez rapidement et à se propager aux feuilles alentours !
    Je me retrouve à piétiner les flammes… et ce qu’il y a en dessous.

    C’est peu glorieux comme histoire mais ça pourra peut-être **ne pas** vous inspirer…
