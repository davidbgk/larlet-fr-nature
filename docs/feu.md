# Le feu

!!! info "Contexte"

    Je me promène dans la forêt boréale (aussi appelée « taïga ») du sud du Québec, principalement les Laurentides, c’est une forêt dense qui n’est pas entretenue.
    Ce n’est pas pour autant une forêt primaire car elle a été exploitée au cours des derniers siècles et les épisodes de sécheresse sont rendus possibles en été.

    Le gros avantage lorsqu’on veut faire du feu dans ce contexte, c’est
    qu’en dehors des lieux très achalendés, on trouve facilement du bois
    en bonnes conditions pour faire un feu. Aussi, l’écorce de bouleau
    est un allume feu exceptionnel.


!!! danger "Avertissement"

    Faire du feu est une activité dangereuse. **Vraiment.** Pour vous et pour tout ce qui vit dans les hectares à la ronde.


## Approche

Je suis de moins en moins enclin à utiliser les foyers existants et _encore moins_ à créer le mien. Il existe un moyen pour ne laisser presqu’aucune trace : utiliser un réchaud à bois.

Il s’agit d’un foyer trans·portable qui permet de faire un feu efficace  dans un environnement contrôlé. Il consomme énormément moins de bois et ne laisse qu’un peu de cendres rapidement rendues invisibles.
Il y a des modèles en Titane à moins de 100g qui se montent en quelques secondes.

Il suffit d’une pierre (un sol dégagé peut suffire aussi) et il peut même être surelevé sur une souche comme ici pour être à la bonne hauteur pour cuisiner ou se réchauffer les mains :

<div class="side-by-side" markdown>
<figure markdown>

![Un réchaud à bois en cours d’usage](images/feu-rechaud.jpg)

<figcaption>
    Notez que c’est davantage sécuritaire avec un sol couvert de feuilles mortes aussi ! La souche, même si c’est du bouleau est très humide.
</figcaption>
</figure>

<figure markdown>

![Un réchaud à bois en cours d’usage (plan large)](images/feu-rechaud-large.jpg)

<figcaption>
    En dézoomant un peu, j’ai délimité un périmètre de feuilles mortes et j’ai mon bois de prêt, suffisamment en retrait.
    Je n’ai plus qu’à m’asseoir sur mon sac-siège.
</figcaption>
</figure>
</div>

!!! hint "En bonus"
    Il est possible le soir de préparer un feu dans le réchaud (une veille d’orage), de le garder au sec dans la tente et de n’avoir besoin que d’une allumette au réveil. Si vous n’êtes pas trop du matin — comme moi — ça aide.


## Préparation

Il faut du bois sec, pas trop attaqué par la vermine et les champignons.
En général, les arbres morts encore dressés ou les branches basses déjà mortes sont les meilleures pistes pour arriver à une combustion optimale.
Si ça se délite au premier coup de scie c’est mal barré, si vous n’arrivez pas à casser une branche car elle se tord, pareil.

Il vaut mieux prévoir à l’avance une quantité raisonnable de bois moyen, c’est généralement ce qui manque et qui ne permet pas de passer de l’allumage à la bûche·tte.
Le feu doit prendre mais surtout être entretenu ! (toute métaphore serait fortuite)

!!! hint "Feu retourné"

    Si vous n’avez pas la même [approche](#approche) et que vous êtes sur un  foyer ouvert, je recommande vivement deux choses :
    
    1. réduire drastiquement la taille du foyer ;
    2. commencer par mettre les bûches au fond du foyer, puis le bois moyen au-dessus, puis enfin votre bois d’allumage tout en haut, l’écorce de bouleau vient s’enflammer entre ces deux étages.

    Le principal gain de cette méthode est de pouvoir faire d’autres choses pendant que le feu est en train de faire des braises dans son coin.
    Par exemple, filtrer l’eau de cuisson… ou couper/fendre plus de bois !

    Vous pouvez voir un exemple de cette méthode en action dans [l’une de mes vidéos hivernales](https://vimeo.com/516515235/45826baa92?ts=323000).
    C’est particulièrement pertinent sur la neige, je suis en train d’explorer l’usage d’un [foyer portable](https://www.wolfandgrizzly.com/products/fire-safe) pour ces cas là. Retour à venir au printemps prochain.


## Allumage

On peut s’amuser en frottant une barre de Ferrocérium mais honnêtement c’est pas super efficace.
Un bon briquet avec des allumettes sèches en _backup_ ça suffit amplement. 
J’ai toujours 2/3 alumettes « tempêtes » et un peu d’amadou pour les cas critiques mais je m’en sers très très rarement (grand vent en hiver, perte de dextérité critique).

C’est un autre avantage d’avoir un foyer portable : il est possible de se mettre à l’abri du vent même lorsque celui-ci se met à tourner !
En réduisant la taille du feu, il est possible de prendre aussi un paravent adapté au réchaud.
En cas de grand vent, il est tout de même recommandé de mettre sa semoule dans l’eau froide et d’attendre que ça gonfle…


## Entretien

C’est la partie la plus difficile, surtout lorsqu’on destine le feu à la cuisine.
Il faut réussir à faire des braises d’un côté tout en maintenant le feu de l’autre.
Avec un réchaud, il y a plusieurs stratégies :

* une alimentation continue avec du petit bois ;
* une bûche coupée en 4 (en mode [bûche scandinave](https://fr.wikipedia.org/wiki/B%C3%BBche_scandinave) mais en mettant chaque coin dans les coins du réchaud, il faut que je fasse une photo) qui procure une longue flambée et qui termine sur des braises.

Selon le moment et le contexte, je choisis l’une ou l’autre, la seconde étant plus appropriée lorsque le feu doit aussi servir à se réchauffer.
La première a l’avantage de ne nécessiter aucun outil de coupe/fendage.

Pour un foyer ouvert, la méthode la plus efficace est de faire un feu avec des bûches parallèles (vs. une pyramide esthétique mais au rendement très mauvais).
En bonus, ça permet de caler la popote à cheval sur deux bûches, l’endroit le plus adapté à la cuisson de par sa stabilité.
Rien n’est pire que de voir son repas finir dans le feu…

!!! hint "Fendre du bois"

    Une bûche entière va avoir du mal à démarrer et mettre très longtemps à s’éteindre.
    La hache est attirante pour l’imaginaire du bucheron mais excessivement dangereuse, surtout lorsqu’on se balade seul·e.

    Il existe une technique qui demande une forme de couteau particulière (lame épaisse allant du manche à la pointe) qui s’appelle [le « batônnage »](https://fr.wikipedia.org/wiki/B%C3%A2tonnage_(bois)).
    Le risque est bien plus réduit et le résultat est équivalent, si pas plus rapide.

<figure markdown>

![Un arbre transformé en chevalet de coupe](images/feu-chevalet.jpg)

<figcaption>
    Et pourquoi ne pas utiliser un arbre comme chevalet pour faciliter la coupe en maintenant la partie à scier ?
</figcaption>
</figure>


## Extinction

Dans un réchaud à bois, tout se consume très rapidement et il ne reste que des cendres faciles à disperser.
Il est déconseillé d’arroser car cela déformerait irrémédiablement le métal selon sa température.

Sur un foyer ouvert, les braises ont pu consumer des racines assez profondemment, il faut vraiment saturer en eau le sol pour être certain que rien ne va pouvoir redémarrer.
Une grosse vessie est rarement suffisante.
