# L’abri

!!! info "Contexte"

    Je vis dans un pays infesté de bibites dès qu’il fait plus de 10°C.
    La moustiquaire n’est pas une option dans mon cas !
    Il n’est pas rare qu’elle se retrouve avec une cinquantaines de moustiques dessus au moment où je m’endors (je vous laisse imaginer le bourdonnement stressant qui en résulte !).


!!! danger "Gaulois"

    Le principal risque en dormant dans la forêt québecoise est de se prendre un arbre sur la tête au cours de la nuit.
    Il faut vraiment faire un tour du campement pour vérifier que l’on est hors de portée et qu’un arbre n’est pas à moitié rongé par un castor ou massacré par un pic.
    Cela peut permettre de choisir son bois pour le [feu](feu.md) le cas échéant.


## Tente

J’ai mis pas mal de temps à trouver une tente qui me convienne.
J’ai finalement opté pour une [AEON Li de chez Tarptent](https://www.tarptent.com/product/aeon-li/) qui combine pas mal d’avantages pour mon contexte :

* légère et compacte,
* suffisamment transparente pour voir ce qu’il se passe à proximité (et même les étoiles !),
* faible espace requis au sol,
* se monte en 2 minutes pour de vrai,
* suffisamment ventilée pour limiter la condensation.


!!! hint "Condensation"

    Pour une personne respirant normalement au cours d’une nuit, il est habituel que l’intérieur de l’habitacle se retrouve humide, voire franchement détrempé si l’humidité ambiante est élevée.

    Il est souvent plus avantageux en terme de séchage d’utiliser un linge (tour de coup ou une lingette !) qui sera plus rapide à sécher ensuite que votre tente repliée dans votre sac.

<figure markdown>

![Une tente devant un lac](images/abri-tente.jpg)

<figcaption>
    Une vue de l’arrière de la tente, c’est sympa au réveil.
</figcaption>
</figure>


J’ai testé une simple moustiquaire sous un _tarp_ mais toutes sortes de bibites arrivaient quand même à passer.
Surtout en laissant le couchage en place toute une journée.

J’ai testé des _bivy_ mais alors il ne faut vraiment pas être claustrophobe, ni avoir trop chaud, ni devoir se changer, etc.


## Tarp

En hiver (de novembre à avril…), j’apprécie de pouvoir utiliser un simple _tarp_ pour contenir les éléments.
Cela donne un grand nombre de configurations possibles.
Associé à un tapis de sol, on peut difficilement faire plus léger.
Cela permet aussi de se créer un abri indépendamment du couchage (pour cuisiner sous l’orage par exemple).

Lorsqu’il fait vraiment froid (< -15°C), je prends un _tarp_ en forme de tipi pour essayer de maintenir un cône de chaleur quitte à perdre un peu en ventilation.
Il ne faut pas oublier de casser la glace qui a été formée par la respiration dans le cône le matin avant de replier !

<figure markdown>

![Un tipi dans la neige](images/abri-tipi.jpg)

<figcaption>
    Un exemple de ce que ça peut donner, décembre 2019.
    Les premiers rayons du soleil sont très attendus !
</figcaption>
</figure>


## Hamac

Je dors sur le côté, ce qui est très restrictif dans ce domaine.
Aussi, l’inconvénient du hamac, c’est qu’il faut de quoi réchauffer la partie basse également avec un _underquilt_ et/ou un matelas approprié.

Quelques fois par an, je prends mon [SkyLite Hammock de chez ENO](https://enonation.ca/collections/hammocks/products/sky-lite-pacific) lorsque je sais que je vais à un endroit qui n’est pas plat.
Les arbres sont rarement un problème dans le coin.
Le principal souci, c’est que cela laisse un espace vraiment réduit lorsqu’on veut être à l’abri des bibites et/ou des éléments.
Aussi, lorsqu’il y a du vent ça peut pas mal balancer !
À la fois le hamac en lui-même mais aussi les arbres support.

<figure markdown>

![Un hamac sur une plateforme](images/abri-hamac.jpg)

<figcaption markdown>
Lors d’une [micro-aventure de _bikepacking_](https://larlet.fr/david/2020/05/20/), le hamac avec un _underquilt_ (ce que je ne recommande pas avec cette forme de hamac, il vaut mieux un matelas thermique).
</figcaption>
</figure>


Une mention honorable pour les autres fabriquants ayant essayé de rendre possible le couchage sur le côté :

* [Cross Hammock](https://crosshammock.com/en/konzept/)
* [Amok Draumr UL](https://amokequipment.com/products/draumr-ultralight-hammock)
* [Helsdon Hammock](https://www.helsdonoutdoors.com/products.html)


## Neige

L’une des contraintes fortes que j’ai sur mes abris, c’est qu’ils doivent être capables de ne pas s’effondrer sous le poids de la neige.

<figure markdown>

![Une tente sous une bonne couche de neige](images/abri-neige.jpg)

<figcaption>
    Mi-octobre (!) 2018, on ne sait jamais à quel moment on va se prendre les premiers flocons… et là on voit bien les limites de ce modèle de tente.
</figcaption>
</figure>
