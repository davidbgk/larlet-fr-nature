# La nourriture

!!! info "Contexte"

    En ayant appris à courir dans la nature, j’ai aussi appris à m’alimenter en continu pour maintenir ma glycémie.
    C’est une chose que je pratique encore le long du chemin mais arrivé au campement ça fait du bien de passer à du plus consistant !


## Combustibles

J’utilise 3 combustibles :

1. le bois, en grande majorité ;
2. les cubes Esbit ;
3. l’alcool avec un container Tangria.

Il y a des réchauds à bois qui permettent d’utiliser les 3 comme le [Firebox Nano](https://www.fireboxstove.com/ultralight-stoves) qui j’apprécie.
Cela permet de partir sur du bois mais d’avoir une solution de secours s’il y a trop de vent par exemple.


<div class="side-by-side" markdown>
<figure markdown>

![Plat de semoule + haloumi.](images/nourriture-semoule-haloumi.jpg)

<figcaption>
    La cuisine au réchaud à alcool permet une finesse de cuisson qui autorise des plats plus raffinés.
    Ici, semoule au haloumi avec oignons.
</figcaption>
</figure>

<figure markdown>

![Plat de riz + haloumi + sésame.](images/nourriture-riz-haloumi-sesame.jpg)

<figcaption>
    Un autre exemple (de mémoire sur un réchaud à bois) avec du riz, du haloumi et du sésame.
</figcaption>
</figure>
</div>

!!! info "Publicité mensongère"

    Je montre mes meilleures photos de bouffe car 80% du temps c’est riz blanc ou semoule.

## Popote

On fait maintenant des contenants en Titane vraiment légers.
Je privillégie de plus en plus des fonds plutôt larges pour la stabilité (sur le feu mais aussi au sol).
Rien de pire que de voir son plat finir par terre ou son eau bouillante éteindre un feu qui a été long à démarrer.

En hiver, il vaut mieux une grosse casserole qui permettra de mettre beaucoup de neige à la fois [pour la faire fondre](froid.md#eau-liquide).


## Kuksa

Je suis très attaché à ce format pour plusieurs raisons :

* c’est une taille acceptable pour une eau chaude mais aussi pour un repas liquide (nouilles, etc) ;
* on sent la chaleur à travers le récipient sans se brûler, ce qui est très important en hiver (pas besoin de manger avec des moufles…) ;
* l’ouverture est la même que sur l’une de mes popottes ce qui me permet de couvrir le temps que ça gonfle/infuse/etc.

Le problème c’est qu’en bois ça a tendance à craquer, surtout avec les chocs thermiques que je leur impose.
Aussi, j’ai trouvé une version hybride avec les modèles que [propose Kupilka](https://www.canadianoutdoorequipment.com/kupilka-cup-large.html) (en version _large_).
C’est du bois + plastique et ça résiste bien au temps.
C’est un peu lourd mais le gain est rentable avec mon usage.

<figure markdown>

![Un chocolat chaud dans une kuksa de bon matin.](images/nourriture-kuksa-matin.jpg)

<figcaption>
    Rien de mieux qu’un lever de soleil avec un bon chocolat chaud en main.
    C’est le moment où j’envisage un compte Instagram de foodie…
</figcaption>
</figure>

!!! hint "Ne pas faire"

    Si vous envisagez des bols en noix de coco (ce dont je me sers pour mon petit déjeuner à la maison), sachez que ça craque en le remplissant avec de l’eau bouillante.
    J’ai perdu un chocolat chaud et retapissé un refuge en apprennant cela…
    C’est dommage car c’est vraiment léger.


## Adaptation

Un régime gras l’hiver pour que la digestion réchauffe.
Une régime salé l’été pour compenser les pertes liées à la sudation.
J’ai mis longtemps à apprendre cela mais se faire une soupe bien grasse avant de s’emmitoufler dans son duvet c’est l’assurance de s’endormir au chaud.

Une autre adaptation, liée au [froid](froid.md), c’est de sélectionner des aliments qui ne vont pas devenir trop durs en gelant.
Par exemple, je me suis déjà fait mal aux dents avec une barre.
Maintenant, je commence par la piétiner…


## Recette hivernale : la NOODLE BOMB 🍜💣💥

_Je ne sais plus où j’avais découvert ça et je crois que c’est une recette de prisonnier à la base…_

Il s’agit de se faire des nouilles asiatiques à partir d’un paquet individuel, jusque là rien de bien fou mais le _twist_ c’est l’ajout de purée déshydratée par dessus. Pour un poids assez minime, on se retrouve avec beaucoup de calories et des nouilles brûlantes qui n’éclaboussent pas de partout. Aussi, ça facilite leur préhension à la fourchette car les baguettes lorsque tu n’as plus de dextérité c’est moins évident… et je ne parle même pas de manger avec des mitaines ! Je remarque aussi que ça facilite la « vaisselle » après coup car sinon les bords peuvent geler avant d’avoir terminé.

!!! hint "Produit miracle !"

    Je recommande le **Grattoir compact GSI Outdoors** pour réussir à finir une casserole sans avoir besoin d’eau pour la vaisselle. Le rapport usage/prix est probablement le plus élevé de tout mon équipement 😅.

    Aussi, puisqu’on en est à ce genre de considérations culinaires, se faire des nouilles épicées et enchaîner avec un chocolat chaud facilite la transition salé/sucré grâce au piment résiduel. Le thé du matin sera un peu plus buvable ensuite.
