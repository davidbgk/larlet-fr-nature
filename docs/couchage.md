# Le couchage

!!! info "Contexte"

    Il faut que je puisse supporter de -20°C à +35°C selon les saisons.
    Il n’est pas rare d’avoir des différences de 25°C au cours de la même journée.
    Bienvenue au Québec !


## Duvet

Il n’y a rien de plus chaud que de la plume à volume/poids compacté équivalent.
Et je crois que nous sommes le seul animal à pouvoir se couvrir de laine _et_ de plume pour dormir.
Autant en profiter…

Pour gagner du poids, le _quilt_ plus ou moins ouvert en arrière permet d’économiser quelques centaines de grammes parfois mais si vous bougez beaucoup ça n’est peut-être pas rentable.
Un de mes duvets a des fermetures éclair sur le dessus qui n’ouvrent qu’une partie du bourrage, c’est assez efficace lorsqu’il y a de grosses différences de températures au cours de la nuit.

Un « sac à viande » plus ou moins thermique vous permettra de ne laver que lui au fil du temps car laver et faire sécher des plumes sans qu’elles ne perdent de leur qualité est très difficile.
C’est aussi plus agréable à même la peau que le contact du revêtement du duvet.

Ajouter un _bivy_ sur un duvet permet de gagner quelques degrés mais ça peut être à double tranchant car il va aussi garder une partie de l’humidité et un duvet mouillé est un duvet froid, voire dangereux.


## Matelas

!!! hint "Mon matelas se dégonfle !"

    Le volume d’air change en fonction de la température (PV=nRT).
    Il faut avoir compris cette physique élémentaire pour choisir un matelas.

    En résumé, plus il est gros et plus il y a de différence de température entre le moment du gonflage et le réveil et… plus vous avez de chance de vous retrouver à même le sol.

Sachant cela, deux stratégies :

1. Utiliser un gonfleur qui utilise l’air ambiant plutôt que celui de vos poumons à 37°C, cela peut être un simple sac poubelle muni d’un élastique ou un [truc plus pro](https://www.thermarest.com/ca/sleeping-pads/sleeping-pad-accessories/blockerlite-pump-sack/13228.html).
2. Regonfler juste avant de se coucher lorsque l’air est au plus froid (et potentiellement dans la nuit).

Il est possible de cumuler les matelas, notamment pour dormir sur de la neige. 
Voir [la page dédiée](froid.md).


<figure markdown>

![Tentative de couchage sur un raft retourné](images/couchage-raft.jpg)

<figcaption>
    Un exemple de ce qu’il ne faut pas faire.
    Le raft contient un très grand volume d’air.
    Au milieu de la nuit, il s’est plié en deux dans le sens de la largeur.
    J’ai appris…
</figcaption>
</figure>


## Coussin

Il s’agit d’un domaine encore irrésolu.

J’ai tenté le gonflable et le _stuff_ sac rempli d’habits mais sans grand succès.
Les chaussures et ou le sac sous le matelas mais tout a tendance à finir par glisser au cours de la nuit.

Lorsque mon duvet a une capuche, c’est quand même plus simple de coincer des habits à cet endroit mais je perds un peu de volume d’air chaud à un endroit critique.

Je n’arrive pas à me résoudre à investir dans un coussin volumineux pour mieux dormir.

Propositions bienvenues !


## La sieste

Emporter un hamac ultra-léger en bonus permet de faire une sieste pour compléter la nuit souvent courte.
Ça surélève également les jambes et facilite la récupération.

<figure markdown>

![Un hamac et mois de dedans](images/couchage-hamac.jpg)

<figcaption>
    Un exemple de sieste improvisée sur une île au milieu de la rivière.
    Pas pire.
</figcaption>
</figure>
