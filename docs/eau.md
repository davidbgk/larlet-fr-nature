# L’eau

!!! info "Contexte"

    Il y a des pays ou l’on peut boire l’eau des cours d’eau/lacs directement.
    Au Québec, c’est un peu limite car ça stagne pas mal, je préfère emporter un filtre.
    Je n’envisage pas du tout de porter toute l’eau dont j’ai besoin pour une sortie complète.
    Mon dos approuve.

Le filtre le plus rapide est de loin le Katadyn BeFree.
Son efficacité est un peu moindre mais dans mon contexte elle me semble être suffisante.
J’ai l’impression qu’il a une action un peu diurétique mais ça pourrait être la vieillesse aussi… ou la ceinture du sac à dos.

J’ai rarement besoin de porter plus de 600 mL d’eau, que je recharge au gré des lacs et torrents que je croise.

Si je suis vraiment en manque d’eau et que j’en croise de la très stagnante, j’ai aussi quelques pastilles de purification dans la pharmacie.

<figure markdown>

![Une photo avec le filtre](images/eau-filtre.jpg)

<figcaption>
    Pour montrer à quoi ressemble le filtre.
    Il y a aussi un petit visiteur du campement.
</figcaption>
</figure>


La gestion de l’eau avec des [températures négatives](froid.md) est un vrai challenge et avec le climat actuel cela ne me permet d’utiliser le filtre que pendant 6 mois (il ne faut surtout pas qu’il gèle !).

Tant qu’il n’y a pas de neige, je fais bouillir l’eau du lac et il me faut un contenant plus important (jusqu’à 1,5 L) car sinon je passe mon temps à faire du [feu](feu.md).

Lorsqu’il y a de la neige/glace à faire fondre, c’est extrêmement long car il faut recharger en permanence la casserole, la neige ayant un très grand volume par rapport à sa forme liquide.
Une fois l’eau bouillante, il est plus avantageux d’avoir un (grand) thermos et de conserver de l’eau liquide et chaude qui sera coupée avec de la neige au cours du périple.
J’arrive ainsi à doubler la quantité d’eau buvable.

Rien de pire que de se sentir déshydraté en étant entouré d’eau… solide !
Ah si, au printemps, entendre couler l’eau sous la neige et ne pas y avoir accès.
