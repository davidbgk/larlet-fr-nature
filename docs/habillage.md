# L’habillage

!!! info "Contexte"

    Il peut y avoir des écarts de températures de l’ordre de 25°C dans la même journée au Québec.
    C’est souvent un casse-tête pour avoir la bonne tenue…

    De manière générale, j’évite de plus en plus les matières synthétiques.
    

!!! warning "Vegan"

    J’ai bien conscience que les matières ci-dessous sont issues de l’exploitation animale.
    Dans ce domaine, j’ai beaucoup de mal à arbitrer ce qui est pire entre de l’exploitation humaine asiatique et des matières non bio-dégradables _ou_ la tonte d’un mouton mérinos en Australie _ou_ le travail d’une peau chassée localement.
    Difficile de réussir à faire une analyse holistique de la chose (CO<sub>2</sub>, éthique, dégradation des écosystèmes, etc).

    J’ai les mêmes questionnements entre un avocat qui vient du Pérou et le poisson que j’essaye de pêcher dans le lac d’à-côté.


## Laine

Il n’y a rien de mieux pour passer du temps dans la forêt.
Avant je préfèrais les doudounes mais elles sont trop fragiles pour traverser le sous-bois sereinement.
Le seul inconvénient majeur est le poids/volume.
Mais sinon, que des qualités :

* respirant lorsqu’il faut et thermique pour autant ;
* très peu d’odeurs à part celle de la laine elle-même ;
* relativement déperlant selon les modèles ;
* garde des propriétés thermiques même humide ;
* relativement _bushproof_ selon les modèles ;
* bien plus silencieux que tout le reste (photo animalière !) ;
* résiste au feu jusqu’à un certain point.

Ah non, il y a un autre inconvénient !
La neige s’y accroche facilement mais selon sa qualité ça peut s’épousseter facilement.
Au cours du temps, l’humidité peut s’évaporer alors que l’habit est en cours d’utilisation et même mouillé ça reste encore thermique, ce qui est non négligeable (bon courage avec une doudoune humide/trempée).

Il y les sous-couches bien connues à base de laine mérinos mais je suis de plus en plus fan des **sur**-couches comme le [Swanndri Ranger Bushshirt Olive](https://www.bushcraftcanada.com/products/detail.cfm?product=2083) qui sont respirantes tout en étant chaudes _et_ solides.
Il est ainsi possible d’accumuler jusqu’à 5 couches sans avoir le problème récurrent d’une couche intermédiaire qui vient couper la propagation de la chaleur ou de la transpiration.
J’en arrive même à porter des pantalons en laine l’hiver (sur des sous-vêtements et collants en laine aussi).

Avantage final : je dors bien mieux avec de la laine dans un duvet qu’avec un vêtement mois respirant comme une doudoune.

!!! bug "Mites"

    Le principal soucis lorsqu’on a beaucoup de laine chez soi, c’est que les (larves de) mites viennent se régaler.
    Je n’ai pas encore trouvé de meilleure méthode que de laisser les vêtements qui sont déjà en mode emmental (avec des trous) pour attirer les larves qui n’iront pas sur les autres habits.
    Ces vêtements sont ensuite pratiques pour ne plus craindre les éclats du feu !


    J’ai tenté la lavande sans grand effet et je ne veux pas mettre de substances chimiques type Naphtaline.
    Si vous avez des recommandations,  elles sont bienvenues.

## Doudoune

C’est probablement le meilleur ratio poids/encombrement/chaleur.
Mais c’est fragile et si on opte pour un revêtement moins fragile autour des plumes c’est tout de suite beaucoup plus lourd et encombrant.
Pour mon usage de la forêt qui est de faire du hors sentiers dans une bonne densité d’arbres et d’arbustes ça me fait préférer la laine, au moins lorsque je suis actif.

Et une fois arrivé au campement, il faut aller chercher du bois, faire du feu, etc, beaucoup d’autres occasions d’abîmer le revêtement.

Ça peut venir supplémenter un duvet, notamment un _quilt_.


## Bibites

La meilleure option que j’ai pu trouver est le [Bug Shirt](https://bugshirt.com/).
Vraiment, je recommande car c’est très couvrant et permet plusieurs options (avec ou sans le filet de visage) tout en restant relativement respirant.
C’est ce qui sauve ma santé mentale en plein été ou lorsqu’il y a des mouches noires et compagnie.

Et puis, si un jour j’ai des ruches… :sweat_smile:


## Pluie

Le plus difficile lorsqu’il pleut est de ne pas finir trempé à l’intérieur de l’habit de pluie en raison de la transpiration (+ humidité ambiante).

J’adopte aujourd’hui une tenue polyvalente :

* des guêtres, bien pratiques pour la rosée du matin ;
* une jupe (bon OK, [un kilt](https://zpacks.com/products/dcf-rain-kilt)) ;
* un poncho découpé qui ne protège que le tronc ;
* un chapeau (style _cowboy_).

Tous ces éléments peuvent s’utiliser indépendamment et ont plusieurs usages possibles (la jupe permet par exemple de poser le sac sur une surface humide, le chapeau de s’abriter du soleil, etc).
C’est suffisamment aéré pour ne pas suffoquer et garder une amplitude de mouvements correcte.

Lorsqu’il fait froid, j’ajoute des moufles très légères et imperméables car avoir le bout des doigts mouillés et gelés ça devient vite critique niveau dextérité (et donc moyen de se réchauffer plus tard).

## Mitaines

Les québécois·es disent mitaines pour dire moufles mais _du coup_ je ne sais pas comment dire que j’apprécie les moufles sans le bout des doigts :thinking_face:.

Jusqu’à une certaine température, c’est très pratique pour garder de la dextérité (jumelles, manipulation photo/vidéo, téléphone tactile, etc).
J’apprécie de pouvoir replier mes doigts à l’intérieur si c’est nécessaire.

<figure markdown>

![J’observe quelque chose avec des jumelles](images/habillage-mitaines.jpg)

<figcaption>
    Parc d’Oka, octobre 2020.
</figcaption>
</figure>

