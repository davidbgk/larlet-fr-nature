# L’entraînement

!!! info "Contexte"

    Je n’ai pas un entraînement au sens strict du terme : je n’ai pas de plan et je m’adapte beaucoup à mes sensations du moment.
    Il n’y a pas de recherche d’esthétique ou d’un poids particulier.
    J’aime bien quand je me raconte que ça ressemble à un jeu.

    Lorsque j’ai un objectif précis par contre, j’adapte l’effort en conséquence dans les semaines/mois précédents.


## Mental

C’est 80% du boulot, peut-être davantage.

Il y a une frontière floue entre avoir la flemme et écouter un corps fatigué.
Tout l’entraînement consiste à jouer avec cette limite pour éviter les blessures tout en acquérant les capacités nécessaires et enviables qui vont permettre d’être en confiance au(x) moment(s) clé(s).

Avoir un objectif précis aide beaucoup.
Être en capacité de visualiser un déroulé fantasmé de l’évènement ou du périple est un gros bonus aussi, ça aide à savoir _pourquoi_ est-ce que l’on s’astreint à cette sortie en particulier.

Plus qu’un objectif, il faut apprendre à apprécier le chemin car il constitue 99% du temps.
Et il faut s’attendre à ce que ce chemin soit long, une nouvelle habitude de vie.

!!! hint "Pro-tip"

    Je ne sais plus où j’ai lu ça mais si vous avez prévu une séance et que vous n’êtes pas motivé·e le jour J, le fait de ne renoncer qu’une fois habillé·e et prêt·e à partir sur le pallier peut faire la différence.


## Préparation Physique Générale

Les pro ont plein de termes techniques comme la VO<sub>2</sub>max, les seuils, le lactate, la cadence, etc.

Pour ma part, j’essaye de faire de l’entraînement utile.
Les courses alimentaire à pied avec un sac à dos, les aller-retours à l’école avec le vélo qui tire 45kg, les remplacements des livreurs pour éviter les derniers kilomètres, une grande partie de mes derniers déménagements, etc.

Tout ce qui se fait dans un environnement non-changeant, statique, climatisé me brise le moral direct.
Il n’y a que la natation pour laquelle je suis contraint d’aller en piscine une bonne partie de l’année et j’essaye de tenir en me rappelant le bonheur de nager dans un lac l’été.

L’alternance des sports pour solliciter différentes parties du corps est essentielle.
J’ai découvert cela en croisant la course à pied et le vélo mais en fait pour être complet ça nécessite bien d’autres efforts.
Les sports du triathlon sont relativement complémentaires dans ce sens.

<figure markdown>

![Je tire un traîneau, en montée, avec l’enfant dedans](images/entrainement-hiver.jpg)

<figcaption>
    Je me souviens m’être exclamé à cette occasion :<br>
    « Le crossfit c’est pour les nullipares ! ».<br>
    Mont-Royal, février 2018.
</figcaption>
</figure>


## Météo

!!! info "Contexte"

    Il y a deux saisons au Québec : l’hiver et les travaux (#privatejoke de montréalais·es).
    Il y a néanmoins du vrai dans cela, l’entre-saison est difficile à gérer en terme d’entraînement : les températures font du yo-yo, le sol est plus ou moins glissant et/ou gelé, les revêtements ne sont pas propres, etc.

Lorsque je suis arrivé au Québec, j’avais besoin de me prouver que je pouvais continuer à m’entraîner quelle que soit la météo.
Entre arrogance et insouciance, j’ai découvert que l’on pouvait faire geler son palais en courant ou qu’il est possible de coincer son détailleur à cause de la glace (bien choisir sa vitesse en descente !).

Avec le temps, j’ai appris à m’adapter et à adopter les sports locaux (patin à glace, ski de fond, raquettes).
Il est possible d’arriver à la même régularité/intensité avec une partie de la prise de risque moindre.


## Régime

!!! warning "Troubles alimentaires"

    Je n’ai pas de troubles alimentaires (malgré mon addiction au chocolat).
    J’ai conscience que ce qui suit peut être — au minimum — frustrant pour des personnes qui doivent déjà lutter au quotidien pour avoir un poids et une alimentation considérés comme sains.
    N’hésitez pas à passer cette section.

Je ne suis pas trop attaché à un poids et j’essaye plutôt de m’adapter à la saison car il y a des températures où le gras est assez utile.
Mes grosses variables d’ajustement sont le chocolat, le fromage et l’alcool.
Ensuite, ça passe principalement par une réduction des parts.
Le fait de se faire à manger à la maison chaque midi est un plus indéniable pour influer sur cette variable d’ajustement. Bon après, c’est beaucoup plus facile avec des habitudes alimentaires plus méditérannéennes qu’américaines !

Je n’ai jamais su si aller s’entraîner à jeûn le matin avait vraiment un intérêt physiologique mais niveau mental ça permet assurément de franchir un cap.

Privillèges++ : il y a un apprentissage de la faim qui n’est pas inintéressant dans ce processus et qui peut resservir dans d’autres contextes (hypoglycémies, réserves mangées par un animal, etc).

:material-bike-fast: En parlant d’hypoglycémie, les plus terribles arrivent sur un vélo.
Il suffit d’un stress brutal (côte, voiture proche, accident, etc) pour que subitement tout le sucre restant soit consommé et que le corps se retrouve à sec sans avoir trop d’avertissements car la consommation continue est relativement basse sur un vélo.
Toujours partir avec une barre.

!!! hint "Lavage de dents"

    C’est ma principale astuce lorsque je veux avoir une influence sur mon poids en réduisant les grignotages : me laver les dents à chaque fois.
    Le temps que ça prend me dissuade assez rapidement et en une semaine je reviens aux repas classiques exclusivement.


## Récupéra(c)tion

Je n’ai plus de montre connectée qui me donne mon temps de récupération.
Je crois que j’ai suffisamment appris à me connaître.
Ou j’en ai l’illusion et c’est déjà ça.

J’ai trouvé une alternative intéressante pour pallier cela :

Je lave mon équipement sous la douche qui suit la sortie.
Tant qu’il n’est pas sec, je ne refais pas le même sport.
Il ne faut pas dépasser une tenue par sport et par saison sinon c’est tricher !

Autre avantage, en plus d’assurer la longévité d’un équipement technique qui passe très mal à la machine, c’est de mieux connaître les forces et faiblesses de cet équipement en le connaissant — littéralement — sous toutes les coutures.

!!! hint "Pro-tip"

    J’utilise une huile essentielle arnica+gaulthérie qui fait des miracles sur les muscles qui ont un peu trop été sollicités.
    À chaque fois que je l’oublie, je suis surpris de son efficacité.


!!! info "Chaise"

    Avoir une [chaise de bureau](https://larlet.fr/david/stream/2018/05/15/) qui ne coupe pas la circulation sanguine semble m’aider dans le domaine.


## Reprise

Lors de ces deux dernières années, j’ai énormément réduit l’intensité de mes entraînements.
Principalement pour ne pas me retrouver en immuno-dépression/fatigue trop poussée au moment d’encaisser la Covid-19.
En ce moment (automne 2022, [post épidode covid](https://larlet.fr/david/2022/07/08/)), je m’y remets et je vois immédiatement les effets bénéfiques en terme de sommeil et de santé mentale.

La contre-partie c’est que c’est d’autant plus dur de ne plus arriver à faire des choses qui semblaient auparavant si faciles.
Il faut une bonne dose d’acceptation !

Mes auto-conseils :

* reprendre vraiment doucement (c’est normal de ne plus arriver à faire plus de 10% de ce qui a déjà été fait, il va falloir repartir de quasi-zéro, le mental est à double-tranchant dans ce contexte) ;
* trouver un objectif très accessible (j’y travaille, si vous cherchez un terrain de jeu [FastestKnownTime](https://fastestknowntime.com/) peut être inspirant) ;
* remettre le mental en route et avec lui l’appréciation de la régularité ;
* réapprendre à s’hydrater correctement pendant l’effort et en récupération ;
* accepter de changer les positions, la souplesse est différente, la musculature aussi (surtout pour un vélo de triathlon).

Selon son rapport au matériel, le mode récompense peut aider à se motiver lors d’une reprise.

!!! warning "Rappel"

    Vous n’êtes probablement pas un athlète élite.
    Essayer de se calquer sur leur(s) pratique(s) est un boulevard vers la blessure et le découragement.
    Dans une moindre mesure, vous n’avez pas les capacités de vos ami·es non plus.

    Ou du moins pas encore :wink:.


## Temps

Tout ce la prend du temps, beaucoup de temps.
C’est aussi prendre le temps de s’écouter, de (re)découvrir son corps, prendre le temps de méditer en mouvement.
Être attentif à son environnement, croiser un renard en courant, rouler en suivant un vol de bernaches, etc.

De manière générale, l’important c’est la régularité plus que l’intensité et il faut apprendre à courir lentement par exemple.
Réussir à tenir une discussion est un bon indicateur de l’intensité de base qu’il faut réussir à avoir.
Vu que je m’entraîne seul, j’essaye d’adopter une allure qui me permettrait d’aller le plus loin possible (juste au-dessus de la marche).

Il est possible de croiser les séances et les sports aussi.
Par exemple une séance longue de course puis une autre de vélo intensive le lendemain.
Avec de meilleures capacités, il devient envisageable de croiser dans la même journée.

!!! book "Surentraînement"

    Il y a quelques années, je me préparais à faire le triathlon de Montréal (distance olympique).
    Une dizaine de jours avant l’évènement, je lève le pied et ça tombe bien car on part en vacances en famille.
    Je me sens très en confiance car je me suis entraîné tout l’été.

    Malheureusement, sur la route du retour on passe par la [Chute-Montmorency](https://www.sepaq.com/destinations/parc-chute-montmorency/) qui dispose des plus beaux escaliers du Québec.
    En plein pic de forme, je ne peux pas résister à enfiler les baskets pour aller les faire en courant, plusieurs fois, trop de fois…
    Le lendemain, j’ai des courbatures terribles qui me feront renoncer à prendre le départ de la course.
    Tristesse.
    Apprentissage.

    Une prochaine fois : laisser les baskets à la maison.


## Apprentissage

Je ne sais pas à quel point je veux partager — et donc orienter — dans ce domaine.
Je vais me contenter de quelques pointeurs mais ça serait dommage de vous divulgâcher un chemin que vous pourriez découvrir par vous même :

* [Courir pieds nus](https://larlet.fr/david/thoughts/#barefoot) pour travailler la posture, les sensations, l’économie, etc ;
* [Pédaler avec rondeur](https://larlet.fr/david/stream/2015/09/07/) pour développer l’ensemble des muscles de la jambe ;
* [Nager en douceur](https://larlet.fr/david/stream/2018/07/06/) pour ne pas lutter contre l’eau mais la traverser ;
* Voir aussi toute [la série #sportembre](https://larlet.fr/david/stream/2015/09/01/) qui date de 2015 mais après relecture je suis toujours en cohérence.


!!! hint "But"

    Comme le disait le sage Vincent :
    
    > Certain·es s’entraînent pour **gagner** (des compétitions) et d’autres pour **perdre** (du poids).
    
    Dans mon cas, c’est principalement pour apprendre et comprendre.
