# Le froid

!!! info "Contexte"

    Lorsqu’en sortant de la voiture, tu sais que tu ne vas pas pouvoir dépasser les -10°C ces prochaines 36 heures, il faut apprendre à s’adapter.
    Un bon équipement ça aide pas mal, encore faut-il avoir appris à s’en servir avant.


## Transpiration

C’est l’ennemi numéro 1.

Pour l’éviter :

* réduire l’allure ;
* privillégier les vêtements respirants (laine) à des coques synthétiques ;
* connaître son corps et se dé·couvrir en conséquence ;
* réduire les ambitions aussi.

!!! info "Carbone"

    En parlant de transpiration, le carbone a l’avantage d’être léger mais surtout de ne pas rester collé à la transpiration en cas de grand froid.
    Pour un choix de bâtons ou d’armature de tente (ou de trépied photo) ça peut être pris en considération…
    Aussi, le carbone ne rouille pas.


## Eau liquide

J’ai déjà parlé de [l’eau par ici](eau.md), lorsque les températures sont négatives c’est un réel challenge de ne pas finir par sucer un glaçon…
Toutes les poches à eau, pipettes, etc sont proscrites pour cette raison.

Un bon thermos permet de garder l’eau chaude plus longtemps.
Le garder dans le duvet permet de s’assurer d’avoir de l’eau liquide le matin.

Faire fondre de la neige (poudreuse) est très long et demande beaucoup d’énergie.
Il faut plusieurs fois le volume de neige pour avoir un volume d’eau, ça dépend de la neige mais vous pouvez tabler sur 5 fois facilement.
Il est recommandé de mettre un fond d’eau dans la casserole pour ne pas que la neige ait un goût de crâmé pas terrible.

Faire un trou à la hache dans la couche de glace d’un lac est rarement rentable.
Et puis, bon courage pour faire autre chose avec votre lame ensuite…

!!! book "Un Mont-Blanc à sec"

    En 2013, je m’initiais à l’alpinisme en allant faire un tour au sommet du Mont-Blanc.
    Ma connaissance/expérience du froid était alors beaucoup plus limitée.
    Je décide de prendre un sac très léger avec une poche à eau dorsale en espérant que ma chaleur corporelle permette de garder cette eau liquide.

    C’était optimiste.

    Il n’a pas fallu une heure pour que la pipette de racordement et son tuyau gèlent complètement, me condamnant à effectuer l’ascension sans une goutte d’eau.

    Arrivée au sommet, ma tentative de boire directement depuis la poche à eau avec les doigts à moitié gelés s’est soldée par une bonne douche.
    Ce jour là, j’ai appris.

    Si vous voulez la [version complète de l’ascension](https://larlet.fr/david/blog/2013/mont-blanc/).



## Extrêmités

Le plus important est de rester au sec à ces endroits aussi et de prendre une pointure de plus (même pour les gants) afin de réchauffer l’air entre la peau et le vêtement.

Lors d’une pause, commencer par se couvrir même si on n’a pas froid sur le coup car [l’hypothermie](securite.md#etre-en-hypothermie) est quasi-immédiate lorsqu’on a transpiré.
Le mouvement, bâtons par exemple, permet d’irriguer jusqu’au bout des membres.
Rester en mouvement est parfois nécessaire.

Souffler sur ses mains est souvent contre-productif car la respiration est pleine d’eau…

En-dessous de -20°C, toujours avoir une couche de vêtements entre la peau et l’extérieur ou les engelures arrivent très vite.
La peau humaine est bien mal adaptée à de tels extrêmes.

!!! hint "Une pause en refuge ?"

    Je n’ai jamais eu aussi froid en randonnée (à ski) qu’après une pause dans un refuge bien chauffé.
    Toute la neige/glace sur mon équipement avait eu le temps de fondre et de retour à -20°C s’est mise à geler et à me faire passer très rapidement en hypothermie.
    Depuis, je mange ma barre _devant_ le refuge.


## Isolation

Avoir un bon duvet ne suffit pas, il faut un (ou plusieurs !) bons matelas aussi.

C’est une erreur classique de penser qu’un duvet côté -20°C (attention, c’est souvent faux) va permettre de passer une bonne nuit sur la neige.
Si le matelas n’est pas également thermique (valeur de R), le froid va passer entre les plumes compressées et vous frigorifier.

!!! hint "Survie"
   
    En conditions de survie, il est possible de faire un tapis de branchage de résineux pour s’isoler de la neige.
    Dans le cas contraire, les arbres aussi essayent de survivre à l’hiver…


!!! info "Bushcraft-style"

    Notez qu’il est possible d’être beaucoup plus créatif/téméraire en terme de couchage. Ici, une tentative de lit surélevé pour éviter d’être au contact du sol gelé :

<figure markdown>

![Un lit surélevé avec des branches](images/froid-lit-sureleve.jpg)

<figcaption>
   Le lit a basculé.
   Je suis tombé sur la glace en pleine nuit.
   Ça fait mal.
   Je me suis pris toutes les bûches sur la tête.
   Double raison de ne pas recommencer ainsi.
</figcaption>
</figure>


## Feu

Faire un [feu](feu.md) sur la neige requiert pas mal d’expérience.
La principale difficulté étant que celui-ci finit par couler !

<div class="side-by-side" markdown>

<figure markdown>

![Un feu allumé sur la neige](images/froid-feu-allumage.jpg)

<figcaption>
   Ici, j’avais préparé le terrain de enlevant toute la neige possible avec les raquettes utilisées comme pelles préallablement.
   1 mètre cube, ça réchauffe une première fois…
</figcaption>
</figure>

<figure markdown>

![Le même feu dans la neige le lendemain](images/froid-feu-lendemain.jpg)

<figcaption>
   On observe le lendemain qu’il a fini par toucher le sol en s’enfonçant progressivement.
</figcaption>
</figure>
</div>

!!! tip "Pro-tip"

    Un briquet est beaucoup plus efficace lorsqu’il n’est pas gelé.
    Je le garde dans une poche intérieur la demi-heure qui précède l’allumage.
    

## Faire tenir le foyer au-dessus de la neige

Pour l’hiver 2022-2023, j’ai décidé d’expérimenter des foyers suspendus qui éviteraient au feu de couler dans la neige.
Je suis vraiment satisfait du résultat mais [le foyer retenu](https://www.wolfandgrizzly.ca/products/fire-safe) fait tout de même un peu moins d’un kilo…
Sa taille permet de se réchauffer et de cuisiner dessus, c’est bien plus économe en bois qu’un foyer ouvert dans la neige.
Ça génère surtout beaucoup moins de fumée/vapeur d’eau.

<div class="side-by-side" markdown>

<figure markdown>

![Un feu allumé sur la neige, dans un foyer](images/froid-feu-suspendu-allumage.jpg)

<figcaption>
   En équilibre relativement stable sur deux bûches (humides) qui correspondent à la base de l’arbre sec débité.
</figcaption>
</figure>

<figure markdown>

![Le même feu dans la neige le lendemain, dans un foyer](images/froid-feu-suspendu-lendemain.jpg)

<figcaption>
   Le lendemain, il reste de la cendre et ça a lentement fondu entre les deux bûches mais le foyer est resté à niveau. Victoire !
</figcaption>
</figure>
</div>

---

Pour l’hiver 2024, je teste le [Firebox FREESTYLE Modular Stove](https://fireboxstove.com/shop/?wpf_filter_cat_1=234&wpf_fbv=1) en position hexagonale car ça tient dans une seule boîte. Pour l’instant j’en suis satisfait compte-tenu du poids. J’ai la possibilité de le convertir en triangle pour du très ponctuel et sinon ça me permet de faire fondre la neige d’un côté tout en me réchauffant de l’autre. Ça conditionne aussi la taille des « bûches » que je peux faire rentrer dedans comparé à du plus petit/traditionnel.

<div class="side-by-side" markdown>

<figure markdown>

![Un feu allumé sur la neige, dans un foyer](images/froid-feu-freestyle.jpg)

<figcaption>
   Ici avec la casserole qui fait un peu plus d’un litre de mémoire.
</figcaption>
</figure>

<figure markdown>

![Un feu allumé sur la neige, dans un foyer](images/froid-feu-freestyle-torche.jpg)

<figcaption>
   Ici en mode réveil où je me gèle, ça permet quand même une belle flambée.
</figcaption>
</figure>

</div>
