# L’enfant

!!! info "Contexte"

    Traditionnellement, on se fait une sortie père-fils à la fin de l’été (moins de bibites, pas encore trop froid).

    Je vais toujours à des endroits que je connais.
    L’objectif est de passer du bon temps et de transmettre à **son** rythme.
    J’essaye aussi d’aller à des endroits où il y a de l’espace autour du campement pour pouvoir se dépenser sans que je sois trop en stress.

!!! book "Jamais trop tôt ?"

    Août 2022, je m’apprête à passer la nuit à côté d’un refuge lorsque je vois débarquer un papa avec un bidon de 4L d’eau dans chaque main et… son bébé de 4 mois sur le ventre.
    Le plus proche parking est à 5 km de marche.

    On passe une soirée très sympathique, à la fraîche (5°C), devant le feu qui garde les biberons au chaud.
    Une belle rencontre qui m’a rappelé que les barrières que l’on se fixe sont dans la tête.


## 5 ans, Labelle

C’est presque du camping de voiture (~200m), on va autour d’un lac sur le tracé du Sentier National.
Il y a des sortes d’emplacements mais ça reste sauvage.

Aucun sac sur son dos.

Ma principale crainte est qu’un coyote l’emporte à l’arrachée.
On se gave de myrtilles, on construit des choses, il lit les Schtroumpfs.
On s’autorise même un petit tour en raft.

<figure markdown>

![Un tabouret avec des myrtilles dessus et nos pieds dessous](images/enfant-5ans.jpg)

<figcaption>
    Environs de Labelle, août 2019.
</figcaption>
</figure>


## 6 ans, Labelle

Cette fois, on fait environ 2 kilomètres de marche avec du dénivelé et un passage assez engagé où il faut passer sur des troncs pour accéder à une île.
J’appréhendais un peu cette traversée mais en me suivant en tenant les bâtons ça passe sereinement.

Tout petit sac à dos.

Il pêche ses premiers petits poissons, on se baigne au milieu des serpents.

<figure markdown>

![Un enfant qui pêche](images/enfant-6ans.jpg)

<figcaption>
    Environs de Labelle, août 2020.
</figcaption>
</figure>


## 7 ans, Ouareau

Cette fois c’est un emplacement mais il est à 3,5 km de la voiture.

Il porte son sac de classe avec une partie de la tente.
C’est pas terrible car ce n’est pas un sac de rando et ça lui scie les épaules.
Je me retrouve à porter deux sacs.

On apprécie le passage du castor en soirée et le petit déjeuner du héron au réveil.

<figure markdown>

![Un enfant qui se gèle le matin](images/enfant-7ans.jpg)

<figcaption>
    Rivière Ouareau, août 2021.
</figcaption>
</figure>

!!! book "Récit"

    J’ai parlé plus longuement de cette sortie [ici](https://larlet.fr/david/2021/08/16/) et [là](https://larlet.fr/david/2021/08/17/).


## 8 ans, Refuge de l’Appel

On choisit de partir un peu plus tard cette année, début octobre pour avoir une autre expérience (et en bonus de jolies couleurs).
Cela me fait opter pour une nuit en refuge et j’ai bien fait car il fait un 0°C bien humide au réveil.
Il fait 5°C à l’intérieur du refuge car j’ai eu la flemme de me relever en pleine nuit mais le poële repart rapidement.
Deux hurlements de loups/coyotes à 6h nous permettent d’apprécier le confort d’un lieu en dur.

Ce qui est nouveau pour cette sortie, c’est qu’il y a une marche d’approche la veille et un sommet (pas trop matinal) le lendemain.
Au final, ça fera une quinzaine de kilomètres et pas mal de dénivelé dans des conditions boueuses assez difficiles.
Un vrai challenge.

Pour autant, c’est vraiment un bon moment d’échange de marcher autant. 
D’une certaine manière, ça me fait réfléchir aux notions de conteur et de ménestrel qui font probablement partie intégrante (naturelle ?) du déplacement.


<figure markdown>

![L’enfant de dos devant un lac et la forêt colorée.](images/enfant-8ans.jpg)

<figcaption>
    Refuge de l’Appel, octobre 2022.
</figcaption>
</figure>
