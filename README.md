# Nature.larlet.fr

## Installation

```
$ git clone https://gitlab.com/davidbgk/larlet-fr-nature.git
$ cd larlet-fr-nature
$ make venv
$ source venv/bin/activate
$ make install dev
$ make serve
```
